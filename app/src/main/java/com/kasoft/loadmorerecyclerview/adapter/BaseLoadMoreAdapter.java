package com.kasoft.loadmorerecyclerview.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kasoft.loadmorerecyclerview.R;

import java.util.List;

/**
 * Created by Khanh on 1/23/2016.
 */
public abstract class BaseLoadMoreAdapter<T> extends RecyclerView.Adapter {

    public static final int VIEW_ITEM = 0;
    public static final int VIEW_PROGRESS = 1;

    protected List<T> data;
    private LoadMoreListener mLoadMoreListener;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean loading, mLoadMoreEnabled = false;

    /**
     * Constructor for LoadMoreAdapter
     *
     * @param data         data of adapter
     * @param recyclerView recyclerView to be attached
     */
    public BaseLoadMoreAdapter(List<T> data, RecyclerView recyclerView) {
        this.data = data;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            if (!mLoadMoreEnabled || isEmpty()) {
                                return;
                            }

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {

                                setLoadMore(true);

                                if (mLoadMoreListener != null) {
                                    mLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            return onCreateViewHolder(parent);
        } else {
            View progressView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_progress, parent, false);
            return new ProgressViewHolder(progressView);
        }
    }

    public abstract RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        } else {
            onBindViewHolder(holder, getItem(position));
        }
    }

    /**
     * Called when bind view holder
     *
     * @param holder item view holder
     * @param item   data of item
     */
    public abstract void onBindViewHolder(RecyclerView.ViewHolder holder, T item);

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    /**
     * @return get List of data in adapter
     */
    public
    @Nullable
    List<T> getData() {
        return data;
    }

    /**
     * Set data for adapter
     *
     * @param data data to set
     */
    public void setData(List<T> data) {
        this.data = data;
    }

    /**
     * Clear all data
     */
    public void clear() {
        if (data != null) {
            data.clear();
        }
    }

    public void addData(List<T> newData) {

        int lastPosition = getItemCount();

        if (data == null)
            data = newData;
        else
            data.addAll(newData);

        notifyItemRangeInserted(lastPosition, getItemCount());
    }

    @Nullable
    public T getItem(int position) {
        if (data == null || position < 0) return null;
        return data.get(position);
    }

    public boolean isEmpty() {
        return data == null || data.isEmpty();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? VIEW_ITEM : VIEW_PROGRESS;
    }

    public T getLastItem() {

        if (isEmpty())
            return null;

        if (loading)
            return getItem(getItemCount() - 2);
        return getItem(getItemCount() - 1);
    }

    /**
     * Set callback when load-more
     *
     * @param loadMoreListener
     */
    public void setLoadMoreListener(LoadMoreListener loadMoreListener) {
        this.mLoadMoreListener = loadMoreListener;
    }

    private void setLoadMore(boolean loadMore) {

        if (loadMore) {
            if (data != null && getItem(getItemCount() - 1) != null) {
                data.add(null);
                notifyItemInserted(getItemCount());
            }
        } else {
            if (!isEmpty() && getItem(getItemCount() - 1) == null) {
                data.remove(getItemCount() - 1);
                notifyItemRemoved(getItemCount());
            }
        }
    }

    /**
     * Enabled or disabled load-more function
     *
     * @param enabled true to enabled load-more
     *                false to disabled
     */
    public void setLoadMoreEnabled(boolean enabled) {
        this.mLoadMoreEnabled = enabled;
    }

    /**
     * Call when load-more data finished
     */
    public void finishLoadMore() {
        if (loading) {
            loading = false;
            setLoadMore(false);
        }
    }

    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }


    public static class ProgressViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public ProgressViewHolder(View itemView) {
            super(itemView);

            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }

    public interface LoadMoreListener {
        void onLoadMore();
    }
}
