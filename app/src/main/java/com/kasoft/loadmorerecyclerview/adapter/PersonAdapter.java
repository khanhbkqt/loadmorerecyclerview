package com.kasoft.loadmorerecyclerview.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kasoft.loadmorerecyclerview.model.Person;
import com.kasoft.loadmorerecyclerview.R;

import java.util.List;

/**
 * Created by Khanh on 1/23/2016.
 */
public class PersonAdapter extends BaseLoadMoreAdapter<Person> {
    /**
     * Constructor for LoadMoreAdapter
     *
     * @param people         data of adapter
     * @param recyclerView recyclerView to be attached
     */
    public PersonAdapter(List<Person> people, RecyclerView recyclerView) {
        super(people, recyclerView);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_person, parent, false);
        return new PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Person item) {
        PersonViewHolder holder = (PersonViewHolder) viewHolder;
        holder.tvName.setText(item.getName());
        holder.tvAge.setText(item.getAge() + " years old");
        holder.tvGender.setText(item.getGender());
    }

    class PersonViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvGender, tvAge;

        public PersonViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvGender = (TextView) itemView.findViewById(R.id.tv_gender);
            tvAge = (TextView) itemView.findViewById(R.id.tv_age);
        }
    }
}
