package com.kasoft.loadmorerecyclerview.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Khanh on 1/23/2016.
 */
public class Person {

    private static int count = 0;

    private String name;
    private String gender;
    private int age;

    public Person(String name, String gender, int age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static List<Person> getPeople() {
        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 10 && count < 35; i++) {
            people.add(new Person("Person #" + ++count, count % 2 == 0 ? "Male" : "Female", new Random().nextInt(100)));
        }
        return people;
    }

    public static void reset() {
        count = 0;
    }
}
