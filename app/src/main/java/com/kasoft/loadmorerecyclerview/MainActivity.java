package com.kasoft.loadmorerecyclerview;

import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kasoft.loadmorerecyclerview.adapter.BaseLoadMoreAdapter;
import com.kasoft.loadmorerecyclerview.adapter.PersonAdapter;
import com.kasoft.loadmorerecyclerview.helpers.VerticalSpaceItemDecoration;
import com.kasoft.loadmorerecyclerview.model.Person;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, BaseLoadMoreAdapter.LoadMoreListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;

    private PersonAdapter mPersonAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(getResources().getDimensionPixelSize(R.dimen.divider_height)));

        mPersonAdapter = new PersonAdapter(null, mRecyclerView);
        mPersonAdapter.setLoadMoreEnabled(true);
        mPersonAdapter.setLoadMoreListener(this);

        mRecyclerView.setAdapter(mPersonAdapter);

        onRefresh();

    }

    @Override
    public void onRefresh() {
        Person.reset();

        new AsyncTask<Void, Void, List<Person>>(){
            @Override
            protected List<Person> doInBackground(Void... params) {
                try {
                    Thread.sleep(1000);
                    return Person.getPeople();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<Person> people) {

                mSwipeRefreshLayout.setRefreshing(false);
                mPersonAdapter.setLoadMoreEnabled(true);

                if (people != null) {

                    int lastItemCount = mPersonAdapter.getItemCount();

                    mPersonAdapter.clear();
                    mPersonAdapter.notifyItemRangeRemoved(0, lastItemCount);
                    mPersonAdapter.addData(people);
                    mPersonAdapter.notifyItemRangeInserted(0, mPersonAdapter.getItemCount());
                }
            }
        }.execute();
    }

    @Override
    public void onLoadMore() {
        new AsyncTask<Void, Void, List<Person>>(){
            @Override
            protected List<Person> doInBackground(Void... params) {
                try {
                    Thread.sleep(1000);
                    return Person.getPeople();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return null;
                }

            }

            @Override
            protected void onPostExecute(List<Person> people) {

                mPersonAdapter.finishLoadMore();

                if (people != null) {

                    int lastItemCount = mPersonAdapter.getItemCount();

                    mPersonAdapter.addData(people);
                    mPersonAdapter.notifyItemRangeInserted(lastItemCount, mPersonAdapter.getItemCount());

                    mPersonAdapter.setLoadMoreEnabled(!people.isEmpty());
                }
            }
        }.execute();
    }
}
